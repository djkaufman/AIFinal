package bot;

import java.util.LinkedList;

public class Piece {
	public int locationRow = -1;
	public int locationCol = -1;
	private char pieceType = 'e';
	private char[][] gameBoard;
	private char[][] pieceBoard;
	public char[][] curPiece;
	private int boardRows;
	private int boardCols;
	
	public Piece(char type, char[][] board, int boardR, int boardC) {
		pieceType = type;
		gameBoard = board;
		//pieceBoard = spawnPiece(board, type);
		spawnPiece(board, type);
		boardRows = boardR;
		boardCols = boardC;
	}
	
	public char getPieceType(){
		return pieceType;
	}
	
	public LinkedList<String> getPath(int destRow, int destCol, int destRot){
		//System.out.println("Cur Row, Col: " + locationRow+", "+locationCol);
		LinkedList<String> path = new LinkedList<String>();
		for(int i = 0; i < destRot; i++){
			System.out.println("add rotation");
			path.add("rr");
		}
		if(destCol < locationCol){
			for(int i = destCol; i < locationCol; i++){
				System.out.println("add left");
				path.add("l");
			}
		}
		else if(destCol > locationCol){
			for(int i = locationCol; i < destCol; i++){
				System.out.println("add right");
				path.add("r");
			}
		}
		
//		if(destRow > locationRow){
//			for(int i = locationRow; i < destRow; i++){
//				System.out.println("add down");
//				path.add("d");
//			}
//		}
		//System.out.println("add drop");
		path.add("dr");
		return path;
	}
	
	// Called immediately after a piece is spawned, before any actions have been taken
	public boolean checkForLoss(){
		int dims = getDimensions(pieceType);
		for(int i = 0; i < dims; i++){
			for(int j = 0; j < dims; j++){
				if(curPiece[i][j] != '-'){
					if(gameBoard[locationRow + i][locationCol + j] != '-'){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public char[][] drop(){
		int beforeLocationRow = -1;
		while(beforeLocationRow != locationRow){
			beforeLocationRow = locationRow;
			moveDown();
		}
		gameBoard = placePiece(gameBoard, pieceType, curPiece, locationRow, locationCol);
		return gameBoard;
	}
	
	public void moveAllLeft(){
		int beforeLocationCol = -1;
		while(beforeLocationCol != locationCol){
			beforeLocationCol = locationCol;
			moveLeft();
		}
	}
	
	public void moveAllDown(){
		int beforeLocationRow = -1;
		while(beforeLocationRow != locationRow){
			beforeLocationRow = locationRow;
			moveDown();
		}
	}
	
	public LinkedList<int[]> getPossiblePlays(){
		LinkedList<int[]> plays = new LinkedList<int[]>();
		//System.out.println("1 =====");
		plays = getPossiblePlaysHelper(plays); // initial rotation
		//System.out.println("2 =====");
		rotateRight();
		plays = getPossiblePlaysHelper(plays); // 90 degrees
		//System.out.println("3 =====");
		rotateRight();
		plays = getPossiblePlaysHelper(plays); // 180 degrees
		//System.out.println("4 =====");
		rotateRight();
		plays = getPossiblePlaysHelper(plays); // 270 degrees
		//System.out.println("5 =====");
		rotateRight();
		//System.out.println("Number of possible plays: " + plays.size());
		return plays;
	}
	
	public LinkedList<int[]> getPossiblePlaysHelper(LinkedList<int[]> plays){
		int initialLocationRow = locationRow;
		int initialLocationCol = locationCol;
		moveAllLeft();
		int beforeLocationCol = -1000;
		//System.out.println("beforeLoc: " + beforeLocationCol +"    locationCol: " + locationCol);
		while(beforeLocationCol != locationCol){
			//System.out.println("while");
			beforeLocationCol = locationCol;
			moveAllDown();
			int[] play = {locationRow, locationCol};
			plays.add(play);
			//renderBoard();
			locationRow = initialLocationRow;
			moveRight();
		}
		locationRow = initialLocationRow;
		locationCol = initialLocationCol;
		//renderBoard();
		return plays;
	}
	
	public void moveLeft(){
		int dims = getDimensions(pieceType);
		for(int i = 0; i < dims; i++){
			for(int j = 0; j < dims; j++){
				if(curPiece[i][j] != '-'){
					if((locationCol+j-1) < 0 || gameBoard[locationRow+i][locationCol+j-1] != '-'){
						return;
					}
					else{
						break;
					}
				}
			}
		}
		locationCol--;
	}
	
	public void moveRight(){
		int dims = getDimensions(pieceType);
		for(int i = 0; i < dims; i++){
			for(int j = dims-1; j >= 0; j--){
				if(curPiece[i][j] != '-'){
//					System.out.println("Checking " + i + ", " + j);
//					System.out.println("lRow: " + locationRow + "    rRow: " + locationCol);
//					System.out.println("locationCol+j+1: " + (locationCol+j+1));
					if((locationCol+j+1) > (boardCols-1) || gameBoard[locationRow+i][locationCol+j+1] != '-'){
						//System.out.println("Location " + (locationRow+i) +", " + (locationCol+j+1) +" is not empty");
						return;
					}
					else{
						//System.out.println("break");
						break;
					}
				}
			}
		}
		locationCol++;
		//pieceBoard = placePiece(gameBoard, pieceType, curPiece, locationRow, locationCol);
	}
	
	public void moveDown(){
		int dims = getDimensions(pieceType);
		for(int j = 0; j < dims; j++){
			for(int i = dims-1; i >= 0; i--){
				if(curPiece[i][j] != '-'){
//					System.out.println("Checking " + i + ", " + j);
//					System.out.println("lRow: " + locationRow + "    rRow: " + locationCol);
//					System.out.println("locationRow+i+1: " + (locationRow+i+1));
					if((locationRow+i+1) > (boardRows - 1) || gameBoard[locationRow+i+1][locationCol+j] != '-'){
						//System.out.println("Location " + (locationRow+i+1) +", " + (locationCol+1) +" is not empty");
						return;
					}
					else{
						//System.out.println("break");
						break;
					}
				}
			}
		}
		locationRow++;
		//pieceBoard = placePiece(gameBoard, pieceType, curPiece, locationRow, locationCol);
	}
	
	public void rotateRight(){
		int dims = getDimensions(pieceType);
		curPiece = rotatePieceRight(curPiece, dims, dims);
		//pieceBoard = placePiece(gameBoard, pieceType, curPiece, locationRow, locationCol);
	}
	
	public void rotateLeft(){
		int dims = getDimensions(pieceType);
		curPiece = rotatePieceLeft(curPiece, dims, dims);
		//pieceBoard = placePiece(gameBoard, pieceType, curPiece, locationRow, locationCol);
	}
	
	public void renderGameBoard(){
		printBoard(gameBoard, boardRows, boardCols);
	}
	
	public void renderBoard(){
		char[][] newBoard = copyBoard(gameBoard, boardRows, boardCols);
		newBoard = placePiece(newBoard, pieceType, curPiece, locationRow, locationCol);
		printBoard(newBoard, boardRows, boardCols);
	}
	
	public void checkForCompleteLines(){
		gameBoard = clearLines(gameBoard);
	}
	
	public char[][] clearLines(char[][] board){
		boolean rowAllEmpty = true;
		boolean rowFull = true;
		char[][] returnBoard = board;
		for(int i = boardRows-1; i >= 0; i--){
			for(int j = 0; j < boardCols; j++){
				//System.out.println("Checking: " + i +", "+j);
				if(returnBoard[i][j] != '^'){
				
					if(returnBoard[i][j] != '-'){
						rowAllEmpty = false;
					}
					else{
						rowFull = false;
					}
				}
				else{
					rowAllEmpty = false;
					rowFull = false;
					j = boardCols;
				}
			}
			if(rowAllEmpty){
				//System.out.println("All empty row encountered; returning");
				return returnBoard;
			}
			if(rowFull){
				//System.out.println("Row "+i +" is full. Clearing");
				returnBoard = moveBoardDown(returnBoard, i);
				i = i + 1;
			}
			rowAllEmpty = true;
			rowFull = true;
		}
		return returnBoard;
	}
	
	public void createUnbreakableLine(){
		gameBoard = moveBoardUp(gameBoard);
		for(int i = 0; i < boardCols; i++){
			gameBoard[boardRows-1][i] = '^';
		}
	}
	
	// moves every row in the board up one
	private char[][] moveBoardUp(char[][] board){
		char[][] returnBoard = board;
		for(int i = 0; i < (boardRows-1); i++){
			for(int j = 0; j < boardCols; j++){
				board[i][j] = board[i+1][j];
			}
		}
		return returnBoard;
	}
	
	
	// moves every row above the given rown down
	private char[][] moveBoardDown(char[][] board, int row){
		char[][] returnBoard = board;
		for(int i = row; i > 0; i--){
			for(int j = 0; j < boardCols; j++){
				returnBoard[i][j] = board[i-1][j];
			}
		}
		return returnBoard;
	}
	
	public char[][] getCopy(){
		return copyBoard(gameBoard, boardRows, boardCols);
	}
	
	public char[][] copyBoard(char[][] board, int rows, int cols){
		char[][] newBoard = new char[rows][cols];
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				newBoard[i][j] = board[i][j];
			}
		}
		return newBoard;
	}
	
	private char[][] rotatePieceRight(char[][] piece, int rows, int columns){
		char[][] rotatedPiece = new char[rows][columns];
		int iCol = 0;
		int iRow = rows-1;
		for(int rRow = 0; rRow < rows; rRow ++){
			for(int rCol = 0; rCol < columns; rCol++){
				rotatedPiece[rRow][rCol] = piece[iRow][iCol];
				if(rotatedPiece[rRow][rCol] != '-' && gameBoard[locationRow + rRow][locationCol + rCol] != '-'){
					//System.out.println("Could not perform turn. Collision at " + (locationRow + rRow)+", "+(locationCol + rCol));
					return piece;
				}
				iRow--;
			}
			iRow = rows-1;
			iCol++;
		}
		return rotatedPiece;
	}

	private char[][] rotatePieceLeft(char[][] piece, int rows, int columns){
		char[][] rotatedPiece = new char[rows][columns];
		int iRow = 0;
		int iCol = columns - 1;
		for(int rCol = 0; rCol < columns; rCol ++){
			for(int rRow = 0; rRow < rows; rRow ++){
				rotatedPiece[rRow][rCol] = piece[iRow][iCol];
				if(rotatedPiece[rRow][rCol] != '-' && gameBoard[locationRow + rRow][locationCol + rCol] != '-'){
					//System.out.println("Could not perform turn. Collision at " + (locationRow + rRow)+", "+(locationCol + rCol));
					return piece;
				}
				iCol--;
			}
			iCol = columns - 1;
			iRow++;
		}
		return rotatedPiece;
	}
	
	private static void printBoard(char[][] board, int rows, int columns){
		//System.out.println("Printing Board");
		int yAxis = 0;
		for(int i = 0; i < rows; i++){
			System.out.print(yAxis);
			if(yAxis < 10){
				System.out.print(" ");
			}
			System.out.print("|");
			yAxis++;
			for(int j = 0; j < columns; j++){
				//System.out.println(i +", " + j);
				System.out.print(board[i][j] +" ");
			}
			System.out.println();
		}
		System.out.print("   ");
		for(int xAxis = 0; xAxis < columns; xAxis++){
			System.out.print(xAxis +" ");
		}
		System.out.println();
	}
	
	private static char[][] createIPiece(){
		char[][] piece = new char[4][4];
		for(int row = 0; row < 4; row++){
			for(int col = 0; col < 4; col++){
				if(row == 1){
					piece[row][col] = 'I';
				}
				else{
					piece[row][col] = '-';
				}
			}
		}
		return piece;
	}
	
	private static char[][] createJPiece(){
		char[][] piece = new char[3][3];
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(i == 0 && j == 0){
					piece[i][j] = 'J';
				}
				else{
					if(i == 1){
						piece[i][j] = 'J';
					}
					else{
						piece[i][j] = '-';
					}
				}
			}
		}
		return piece;
	}
	
	private static char[][] createLPiece(){
		char[][] piece = new char[3][3];
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(i == 0 && j == 2){
					piece[i][j] = 'L';
				}
				else{
					if(i == 1){
						piece[i][j] = 'L';
					}
					else{
						piece[i][j] = '-';
					}
				}
				}
			}
		return piece;
	}
	
	private static char[][] createOPiece(){
		char[][] piece = new char[2][2];
		piece[0][0] = 'O';
		piece[0][1] = 'O';
		piece[1][0] = 'O';
		piece[1][1] = 'O';
		return piece;
	}
	
	private static char[][] createSPiece(){
		char[][] piece = new char[3][3];
		piece[0][0] = '-';
		piece[0][1] = 'S';
		piece[0][2] = 'S';
		piece[1][0] = 'S';
		piece[1][1] = 'S';
		piece[1][2] = '-';
		for(int i = 0; i < 3; i++){
			piece[2][i] = '-';
		}
		return piece;
	}
	
	private static char[][] createTPiece(){
		char[][] piece = new char[3][3];
		piece[0][0] = '-';
		piece[0][1] = 'T';
		piece[0][2] = '-';
		for(int i = 0; i < 3; i++){
			piece[1][i] = 'T';
			piece[2][i] = '-';
		}
		return piece;
	}
	
	private static char[][] createZPiece(){
		char[][] piece = new char[3][3];
		piece[0][0] = 'Z';
		piece[0][1] = 'Z';
		piece[0][2] = '-';
		piece[1][0] = '-';
		piece[1][1] = 'Z';
		piece[1][2] = 'Z';
		for(int i = 0; i < 3; i++){
			piece[2][i] = '-';
		}
		return piece;
	}
	
	// Use to spawn a new piece on the board at the top
		private  void spawnPiece(char[][] initialBoard, char piece){
			//System.out.println("Piece type: "+piece);
			char[][] board = initialBoard;
			switch(piece){
				case 'O':
					//System.out.println("Spawned. O Piece.");
					curPiece = createOPiece();
					//board = placePiece(board, 'O', curPiece, 0, 4);
					locationRow = 0;
					locationCol = 4;
					return;
					//return board;
				default:
					//System.out.println("Spawned. Not an O Piece. It's a " + piece);
					curPiece = createPiece(piece);
					//board = placePiece(board, piece, curPiece, 0, 3);
					locationRow = 0;
					locationCol = 3;
					return;
					//return board;
			}
		}
		
		private static char[][] createPiece(char piece){
			switch(piece){
			case 'O':
				return createOPiece();
			case 'I':
				return createIPiece();
			case 'J':
				return createJPiece();
			case 'L':
				return createLPiece();
			case 'S':
				return createSPiece();
			case 'T':
				return createTPiece();
			case 'Z':
				return createZPiece();
			default:
				System.out.println("ERROR. INVALID PIECE.");
				return new char[1][1];
			}
		}
		
		// places a piece at the given location
		public char[][] placePiece(char[][] initialBoard, char pieceType, char[][] piece, int row, int col){
			int rows = getDimensions(pieceType);
			int cols = row;
			boolean collisions = false;
			char[][] board = initialBoard;
			for(int i = 0; i < rows; i++){
				for(int j = 0; j < rows; j++){
					if(piece[i][j] != '-'){
						//System.out.println("Placing " + piece[i][j]+" at " + (i+row) + ", " + (j + col));
						if(i+row >= 0 && i+row < boardRows && j+col >= 0 && j+col < boardCols){
							board[i+row][j+col] = piece[i][j];}
					}
				}
			}
			return board;
			
		}
		
		public char[][] removePiece(char[][] initialBoard, char pieceType, char[][] piece, int row, int col){
			int rows = getDimensions(pieceType);
			int cols = row;
			boolean collisions = false;
			char[][] board = initialBoard;
			for(int i = 0; i < rows; i++){
				for(int j = 0; j < rows; j++){
					if(piece[i][j] != '-'){
						//System.out.println("Placing " + piece[i][j]+" at " + (i+row) + ", " + (j + col));
						if(i+row >= 0 && i+row < boardRows && j+col >= 0 && j+col < boardCols){
							board[i+row][j+col] = '-';}
					}
				}
			}
			return board;
			
		}
		
		private static int getDimensions(char piece){
			switch(piece){
				case 'I':
					return 4;
				case 'O':
					return 2;
				default:
					return 3;
			}
		}
	
}
