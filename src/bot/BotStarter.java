// Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import field.Field;
import field.ShapeType;
import moves.MoveType;
import java.util.Scanner;

/**
 * BotStarter class
 * 
 * This class is where the main logic should be. Implement getMoves() to
 * return something better than random moves.
 * 
 * @author Jim van Eeden <jim@starapple.nl>
 */

public class BotStarter {

	public BotStarter() {}
	
	/**
	 * Returns a random amount of random moves
	 * @param state : current state of the bot
	 * @param timeout : time to respond
	 * @return : a list of moves to execute
	 */
	
	
	public ArrayList<MoveType> getMoves(BotState state, long timeout) {
		
		ArrayList<MoveType> moves = new ArrayList<MoveType>();
		ShapeType currentShape = state.getCurrentShape();
		Field myField = state.getMyField();
		int rows = myField.getHeight();
		//System.out.println("Rows: " + rows);
		int columns = myField.getWidth();
		//System.out.println("Columns: " + columns);
		char[][] field = new char[rows+1][columns];
		field = initializeTetrisBoard(field, rows+1, columns);
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < columns; j++){
				int blockType = myField.getCell(j, i).getState().getCode();
				switch(blockType){
				case 0:
					field[i+1][j] = '-';
					break;
				case 1:
					break;
				case 2:
					field[i+1][j] = 'B';
					break;
				case 3:
					field[i+1][j] = '^';
					break;
				default:
					field[i+1][j] = '-';
					break;
				}
			}
		}
		char type = 'e';
		// I, J, L, O, S, T, Z, NONE;
		switch(currentShape.name()){
		case"I":
			//System.out.println("-I");
			type = 'I';
			break;
		case"J":
			//System.out.println("-J");
			type = 'J';
			break;
		case"L":
			//System.out.println("-L");
			type = 'L';
			break;
		case"O":
			//System.out.println("-O");
			type = 'O';
			break;
		case"S":
			//System.out.println("-S");
			type = 'S';
			break;
		case"T":
			//System.out.println("-T");
			type = 'T';
			break;
		case"Z":
			//System.out.println("-Z");
			type = 'Z';
			break;
		default:
			//System.out.println("DEFAULT");
			type = '?';
			break;
		
		}
		Piece curPiece = new Piece(type, field, rows+1, columns);
		LinkedList<String> actions = getActions(field, rows+1, columns, curPiece, 'O'); // 'O' is wrong
		moves = translate(actions);
		List<MoveType> allMoves = Collections.unmodifiableList(Arrays.asList(MoveType.values()));
		//moves.add(allMoves.get(1));
		
//		ArrayList<MoveType> moves = new ArrayList<MoveType>();
//		List<MoveType> allMoves = Collections.unmodifiableList(Arrays.asList(MoveType.values()));
//		moves.add(allMoves.get(1));
		return moves;
		
	}
	
	public ArrayList<MoveType> translate(LinkedList<String> actions){
		ListIterator<String> iterator = actions.listIterator();
		ArrayList<MoveType> moves = new ArrayList<MoveType>();
		List<MoveType> allMoves = Collections.unmodifiableList(Arrays.asList(MoveType.values()));
		while(iterator.hasNext()){
//			DOWN, LEFT, RIGHT, TURNLEFT, TURNRIGHT
//			 , DROP, SKIP;
			String move = iterator.next();
			//System.out.println("Switch");
			switch(move){
			case"l":
				moves.add(allMoves.get(1));
				break;
			case"r":
				moves.add(allMoves.get(2));
				break;
			case"d":
				moves.add(allMoves.get(0));
				break;
			case"rr":
				moves.add(allMoves.get(4));
				break;
			case"rl":
				moves.add(allMoves.get(3));
				break;
			case"dr":
				moves.add(allMoves.get(5));
				break;
			}
		}
		moves.add(allMoves.get(2));
		return moves;
	}
	
	
	/*
 	Shape types: I, J, L, O, S, T, Z
 	Tetris Board Notation
 	-: Empty location
 	I: I shape
 	J: J shape
 	L: L shape
 	O: O shape
 	S: S shape
 	T: T shape
 	Z: Z shape
 	
 	Example board:
 	
 	   0 1 2 3 4 5 6 7 8 9
 	   ___________________
 	20|- - - - - - - - - -
 	19|- - - - - - - - - -
 	18|- - - - - - - - - -
 	17|- - - - - - - - - -
 	16|- - - - - - - - - -
 	15|- - - - - - - - - -
 	14|- - - - - - - - - -
 	13|- - - - - - - - - -
 	12|- - - - - - - - - -
 	11|- - - - - - - - - -
 	10|- - - - - - - - - -
 	9 |- - - - - - - - - -
 	8 |- - - - - - - - - -
 	7 |- - - - - - - - - -
 	6 |- - - - - - - - - -
 	5 |- - - - - - - - - -
 	4 |I - - - - - T - - -
 	3 |I - - S S - T T - -
 	2 |I - S S - - T J J -
 	1 |I - J J J - - J O O
 	0 |I I I I J - - J O O
 */

// takes in a piece and moves it in a given direction
//movePiece(currentPiece, currentBoard, direction){
//	
//}

// takes in a tetris board and clears the line at the given row
//clearLine(string[][] board, int row){
//	for(int i = 0; i < columns; i++){
//		board[row][i] = '-';
//	}
//}

// takes in a tetris board and moves all blocks above the given row down. Used after a line is cleared
//moveBlocksDown(string[][] board, int row){
//	for(int col = 0; col < columnAmount; col++){
//		for(int j = row; j < rowCount - 1){
//			board[j][col] = board[j+1][col];
//			board[j+1][col] = '-';
//		}
//	}
//}

public static char[][] initializeTetrisBoard(char[][] initialBoard, int rows, int columns){
	//System.out.println("Initializing Board");
	char[][] board = initialBoard;
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < columns; j++){
			board[i][j] = '-';
		}
	}
	return board;
}

public static void printBoard(char[][] board, int rows, int columns){
	System.out.println("Printing Board");
	int yAxis = 0;
	for(int i = 0; i < rows; i++){
		System.out.print(yAxis);
		if(yAxis < 10){
			System.out.print(" ");
		}
		System.out.print("|");
		yAxis++;
		for(int j = 0; j < columns; j++){
			//System.out.println(i +", " + j);
			System.out.print(board[i][j] +" ");
		}
		System.out.println();
	}
	System.out.print("   ");
	for(int xAxis = 0; xAxis < columns; xAxis++){
		System.out.print(xAxis +" ");
	}
	System.out.println();
}

// takes in a shapeType and a rotation and returns the shape determined by the rotation
//getShape(shapetype, rotation){
//	
//}

// Takes in a board and judges it based on its characteristics (height, holes, lines completed, etc). Might want to give points to planning multiple line clears.
//getScore(board){
//	
//}

// takes in a board and a shape and then returns a list of all of the possible moves of that shape, including different rotations. Keeps track of the moves to get the piece to where it needs to be
//validPositions(Board, shape){
//	
	
	
	
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public static char choosePiece(int id){
		switch(id){
		case 1:
			return 'I';
		case 2:
			return 'J';
		case 3:
			return 'L';
		case 4:
			return 'O';
		case 5:
			return 'S';
		case 6:
			return 'T';
		case 7:
			return 'Z';
		default:
			return 'e';
		}
	}
	
	//public static char[][] placePiece(char piece, char[][] board)

	public static void main(String[] args)
	{
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}
	
	/*
	public static void main(String[] args)
	{
//		BotParser parser = new BotParser(new BotStarter());
//		parser.run();
		Scanner scan = new Scanner(System.in);
		int rows = 21;
		int columns = 10;
		int round = 1;
		char[][] board = new char[rows][columns];
		board = initializeTetrisBoard(board, rows, columns);
		printBoard(board, rows, columns);
		Piece curPiece = new Piece(choosePiece(randInt(1, 7)), board, rows, columns);
		char nextPiece = choosePiece(randInt(1, 7));
		String command = "n";
		String state = "bot";
		
		if(state.equals("play")){
			while(true){
				System.out.println("Round: " + round);
				System.out.println("Next Piece: " + nextPiece);
				System.out.println("Next Piece: " + nextPiece);
				System.out.println("Aggregate Height: " + aggregateHeight(board, rows, columns));
				System.out.println("Bumpiness: " + bumpiness(board, rows, columns));
				System.out.println("Holes: " + holes(board, rows, columns));
				curPiece.renderBoard();
				command = scan.next();
				if(command.equals("rr")){
					curPiece.rotateRight();
				}
				else if(command.equals("ai")){
					//String[] actions = getActions(board, rows, columns, curPiece, nextPiece);
				}
				else if(command.equals("rl")){
					curPiece.rotateLeft();
				}
				else if(command.equals("down")){
					curPiece.moveDown();
				}
				else if(command.equals("left")){
					curPiece.moveLeft();
				}
				else if (command.equals("right")){
					curPiece.moveRight();
				}
				else if (command.equals("renderGame")){
					curPiece.renderGameBoard();
				}
				else if(command.equals("quit")){
					break;
				}
				else if(command.equals("drop")){
					board = curPiece.drop();
					round++;
					if(round%15 == 0){
						curPiece.createUnbreakableLine();
					}
					//curPiece = new Piece(choosePiece(1), board, rows, columns);
					curPiece = new Piece(nextPiece, board, rows, columns);
					nextPiece = choosePiece(randInt(1, 7));
					if(curPiece.checkForLoss()){
						System.out.println("You Lost!");
						break;
					}
				}
				
				curPiece.checkForCompleteLines();
			}
		}
		else if(state.equals("bot")){
			System.out.println("Bot playing");
			System.out.println("Round: " + round);
			
			boolean gameOver = false;
			while(!gameOver){
			//System.out.println("Possible Plays ===========");
			System.out.println("Next Piece: " + nextPiece);
			System.out.println("Aggregate Height: " + aggregateHeight(board, rows, columns));
			System.out.println("Bumpiness: " + bumpiness(board, rows, columns));
			System.out.println("Holes: " + holes(board, rows, columns));
			//LinkedList<int[]> possiblePlays = curPiece.getPossiblePlays();
			//System.out.println("End Possible Plays ===========");
			LinkedList<String> actions = getActions(board, rows, columns, curPiece, nextPiece);
			ListIterator<String> iterator = actions.listIterator();
			while(iterator.hasNext()){
				curPiece.renderBoard();
				command = scan.next();
				if(command.equals("n")){
					String action = iterator.next();
					switch(action){
					case"rl":
						curPiece.rotateLeft();
							break;
					case"rr":
						curPiece.rotateRight();
						break;
					case"l":
						curPiece.moveLeft();
						break;
					case"r":
						curPiece.moveRight();
						break;
					case"d":
						curPiece.moveDown();
						break;
					case"dr":
						curPiece.drop();
						board = curPiece.drop();
						round++;
						if(round%15 == 0){
							curPiece.createUnbreakableLine();
						}
						curPiece = new Piece(nextPiece, board, rows, columns);
						nextPiece = choosePiece(randInt(1, 7));
						if(curPiece.checkForLoss()){
							System.out.println("You Lost!");
							gameOver = true;
							break;
						}
						break;
					}
					curPiece.checkForCompleteLines();
				}
//				else{
//					if(i > 0){
//						i = i - 1;
//					}
////				}
//			}
			
//			for(int i = 0; i < actions.length; i++){
//				curPiece.renderBoard();
//				command = scan.next();
//				if(command.equals("n")){
//					String action = actions[i];
//					switch(action){
//					case"rl":
//						curPiece.rotateLeft();
//							break;
//					case"rr":
//						curPiece.rotateRight();
//						break;
//					case"l":
//						curPiece.moveLeft();
//						break;
//					case"r":
//						curPiece.moveRight();
//						break;
//					case"d":
//						curPiece.moveDown();
//						break;
//					case"dr":
//						curPiece.drop();
//						board = curPiece.drop();
//						round++;
//						if(round%15 == 0){
//							curPiece.createUnbreakableLine();
//						}
//						curPiece = new Piece(nextPiece, board, rows, columns);
//						nextPiece = choosePiece(randInt(1, 7));
//						if(curPiece.checkForLoss()){
//							System.out.println("You Lost!");
//							gameOver = true;
//							break;
//						}
//						break;
//					}
//				}
//				else{
//					if(i > 0){
//						i = i - 1;
//					}
//				}
//			}
			
			curPiece.renderGameBoard();
			}
		}}
		else if(state.equals("test")){
			curPiece.renderBoard();
		}
		//board = spawnPiece(board, 'O');
		//printBoard(board, rows, columns);
	}
	//==============================
	*/
	
	public static double getScore(char[][] board, int rows, int columns, double a, double b, double c, double d){
		return (a*aggregateHeight(board, rows, columns) + b*completeLines(board, rows, columns) + c*holes(board, rows, columns) + d*bumpiness(board, rows, columns) /*+ e*columnHeight(board, rows, column)*/);
	}
	
	public static int completeLines(char[][] board, int rows, int columns){
		int completeLines = 0;
		for(int i = (rows-1); i >= 0; i--){
			if(lineComplete(board, i, columns)){
				completeLines++;
			}
		}
		return completeLines;
	}
	
	public static boolean lineComplete(char[][] board, int row, int columns){
		for(int i = 0; i < columns; i++){
			if(board[row][i] == '-' || board[row][i] == '^'){
				return false;
			}
		}
		return true;
	}
	
	// calculates the number of holes in the board
	public static int holes(char[][] board, int rows, int columns){
		int boardHoles = 0;
		for(int i = 0; i < columns; i++){
			boardHoles = boardHoles + columnsHoles(board, rows, i);
		}
		return boardHoles;
	}
	
	// calculates the number of holes in a column
	public static int columnsHoles(char[][] board, int rows, int column){
		int holes = 0;
		boolean lookForHoles = false;
		for(int i = 0; i < rows; i++){
			if(board[i][column] != '-'){
				lookForHoles = true;
			}
			else{
				if(lookForHoles){
					holes++;
				}
			}
		}
		return holes;
	}
	
	public static int bumpiness(char[][] board, int rows, int columns){
		int bumpiness = 0;
//		int firstHeight = columnHeight(board, rows, 0);
//		int secondHeight = columnHeight(board, rows, 1);
		for(int i = 1; i < columns; i++){
			int firstHeight = columnHeight(board, rows, (i-1));
			int secondHeight = columnHeight(board, rows, i);
			//System.out.println("firstHeight: "+firstHeight +"       secondHeight: "+secondHeight);
			bumpiness = bumpiness + (secondHeight - firstHeight);
		}
		return bumpiness;
	}
	
	public static int aggregateHeight(char[][] board, int rows, int columns){
		int aggHeight = 0;
		for(int i = 0; i < columns; i++){
			aggHeight = aggHeight + columnHeight(board, rows, i);
		}
		return aggHeight;
	}
	
	// gets the height of a given column in a given board
	public static int columnHeight(char[][] board, int rows, int column){
		int height = 0;
		for(int i = 0; i<rows; i++){
			if(board[i][column] != '-'){
				height = rows - i;
				return height;
			}
		}
		return height;
	}
	

	
	public static LinkedList<String> getActions(char[][] board, int rows, int columns, Piece curPiece, char nextPiece){
		LinkedList<String> actions = new LinkedList<String>();
		LinkedList<int[]> plays = new LinkedList<int[]>();
		int bestRowLoc = -100;
		int bestColLoc = -100;
		int bestRotLevel = 0;
		double bestScore = -100;
		char[][] bestBoard = board;
		for(int i = 0; i < 4; i++){
			plays = curPiece.getPossiblePlaysHelper(plays);
			char[][] boardCopy = curPiece.getCopy();
			ListIterator<int[]> iterator = plays.listIterator();
			while(iterator.hasNext()){
				//System.out.println("==");
				int[] play = iterator.next();
				int rowLocation = play[0];
				int colLocation = play[1];
				boardCopy = curPiece.placePiece(boardCopy, curPiece.getPieceType(), curPiece.curPiece, rowLocation, colLocation);
				//System.out.println("PLACING PIECE");
				//printBoard(boardCopy, rows, columns);
				double score = getScore(boardCopy, rows, columns, -0.510066, .760666, -0.35663, -0.184483);
				if(score > bestScore){
					//System.out.println("Best so far: "+score);
					bestScore = score;
					bestBoard = curPiece.copyBoard(boardCopy, rows, columns);
					bestRowLoc = rowLocation;
					bestColLoc = colLocation;
					bestRotLevel = i;
				}
				//System.out.println("REMOVING PIECE");
				boardCopy = curPiece.removePiece(boardCopy, curPiece.getPieceType(), curPiece.curPiece, rowLocation, colLocation);
				//printBoard(boardCopy, rows, columns);
			}
			plays = new LinkedList<int[]>();
			curPiece.rotateRight();
		}
		//System.out.println("Best Row Loc: " + bestRowLoc +"   Best Col Loc: "+bestColLoc);
		actions = curPiece.getPath(bestRowLoc, bestColLoc, bestRotLevel);
		//curPiece.rotateRight();
		//System.out.println("BEST SCORED BOARD: " + bestScore);
		//printBoard(bestBoard, rows, columns);
		String[] things = {"l", "dr"};
		return actions;
	}
}
